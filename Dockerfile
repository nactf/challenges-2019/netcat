FROM debian:buster

RUN apt-get update && apt-get install -y \
		socat \
	&& rm -rf /var/lib/apt/lists/*

RUN mkdir /challenge
WORKDIR /challenge

COPY flag.txt ./
COPY challenge.sh ./

EXPOSE 12345
CMD socat TCP-LISTEN:12345,reuseaddr,fork,su=nobody EXEC:./challenge.sh,stderr
